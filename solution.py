#!/usr/bin/python3

"""
Implement a simple consistent hash, mapping an example set of objects
to a set of storage devices.  Each storage device is represented
by several notional "chunks" each with their own hash code.

Using a representative set of object sizes, demonstrate the device
usage imbalance that can occur.
"""

import bisect
import copy
import datetime
import hashlib
import numpy as np
import os
import unittest
from collections import namedtuple

try:
    import cPickle as pickle
except ImportError:
    import pickle


class DeviceMapper(object):
    """
    Represent a chunk mapping, mapping keys to a device.
    """

    def __init__(self):
        # We separate lists of hash values for chunks and devices they map to, so we can easily bisect
        self._device_count = 0

        # List of chunk hashes, formed from the hash of 'osdN:C'
        self._chunk_map = []

        # Parallel list, with the device number for each hash
        self._device_map = []

        # Map of device index to list of chunks; Initialized by populate_maps
        self.chunks_by_device = {}

        # Map of device index to device size (as bytes); Initialized by populate_maps
        self.device_size_by_index = {}

    def create(self, chunks, device_count):
        """
        Create a fresh device mapping.
        :param chunks: The number of chunks to include.
        :param device_count: The number of devices to include.
        """
        self._device_count = device_count
        self._chunk_map = []
        self._device_map = []

        # Quicker to sort once, rather than insert into sorted order
        mapping = []
        for device in range(device_count):
            for chunk in range(chunks):
                name = 'osd{}:{}'.format(device, chunk)
                hash_value = md5_hash(name)
                mapping.append((hash_value, device))
        mapping.sort()

        for hash_value, device in mapping:
            self._chunk_map.append(hash_value)
            self._device_map.append(device)


    def get_state(self):
        """
        Get the mapping state to send to other devices on the theoretical network.
        :return: tuple(device_count, chunk_map, device_map)
        """
        return self._device_count, self._chunk_map, self._device_map

    def set_state(self, state):
        """
        Set the chunk and device mapping state.
        :param state: tuple(device_count, chunk_map, device_map)
        """
        self._device_count, self._chunk_map, self._device_map = state

    def locate_chunk(self, hash_value):
        """
        Locate the chunk a file is on.
        :param hash_value: The hashed file name.
        :return: The chunk ID.
        """
        chunk_id = bisect.bisect(self._chunk_map, hash_value)
        if chunk_id == len(self._device_map):
            chunk_id = 0
        return chunk_id

    def locate_device(self, hash_value):
        """
        Locate the device a file is on.
        :param hash_value: The hashed file name.
        :return: The device ID.
        """
        return self._device_map[self.locate_chunk(hash_value)]

    def populate_maps(self, file_data):
        Chunk = namedtuple('Chunk', 'hash size')
        chunk_size_by_chunk_hash = {}
        device_index_by_chunk_hash = {}
        used = np.zeros(self._device_count, dtype=np.int64)
        # Fill chunk_size_by_chunk_hash and device_index_by_chunk_hash
        for file_hash, file_size in file_data:
            device_index = self.locate_device(file_hash)
            used[device_index] += file_size
            self.device_size_by_index[device_index] = used[device_index]
            chunk_hash = self._chunk_map[self.locate_chunk(file_hash)]
            device_index_by_chunk_hash[chunk_hash] = device_index
            if chunk_hash in chunk_size_by_chunk_hash:
                chunk_size_by_chunk_hash[chunk_hash] += file_size
            else:
                chunk_size_by_chunk_hash[chunk_hash] = file_size
        # Fill chunks_by_device to be used later when redistributing chunks
        # This dictionary has each key as device index and value as list of Chunks
        for chunk_hash_value in chunk_size_by_chunk_hash:
            device = device_index_by_chunk_hash[chunk_hash_value]
            chunk_size = chunk_size_by_chunk_hash[chunk_hash_value]
            if device in self.chunks_by_device:
                self.chunks_by_device[device].append(Chunk(chunk_hash_value, chunk_size))
            else:
                self.chunks_by_device[device] = [Chunk(chunk_hash_value, chunk_size)]

    def rebalance(self, file_data, device_size, max_load_factor):
        """
        Rebalance the data on each device to bring each device's total load under the max_load_factor.
        :param file_data: list(tuple(file hash, file size)) mappings.
        :param device_size: int The size of each device.
        :param max_load_factor: The maximum load per device in range [0.0 no data, 1.0 full disk]
        :return: The total bytes moved.
        
        The logic is simple:
        1. Initialize dictionaries with populate_maps(file_data) to make traversing simple and efficient
        2. Sort devices by their usage size
        3. While the percent usage of max device > max_load_factor:
            *  Sort chunks in max device low -> high
            *  While the percent usage of max device  > max_load_factor:
                - Get the smallest (first) chunk, move it from max 
                  to min device
            *  Sort devices by their usage size
        4. Return the updated state
        """
        self.populate_maps(file_data)
        # Sort devices to determine max and min devices
        devices_sorted = sorted(self.device_size_by_index.items(), key=lambda x: x[1])  # x[1] is the device size
        max_device_index, max_device_size = devices_sorted[-1]
        min_device_index, min_device_size = devices_sorted[0]
        # Used in the final state update
        updated_device_by_chunk_hash = {}
        # Return value
        total_bytes_moved = 0
        # Iteratively check until max device utilization < max load factor
        while (max_device_size / device_size) >= max_load_factor:
            chunks_on_max_device = self.chunks_by_device[max_device_index]
            chunks_on_min_device = self.chunks_by_device[min_device_index]
            bytes_on_max_device = sum(chunk.size for chunk in chunks_on_max_device)
            chunks_on_max_device.sort(key=lambda chunk: chunk.size)
            # Move chunks in max device to min device until percent_usage is < 90%
            while (bytes_on_max_device / device_size) >= max_load_factor:
                chunk_to_move = chunks_on_max_device[0]
                chunks_on_max_device.remove(chunk_to_move)
                chunks_on_min_device.append(chunk_to_move)
                total_bytes_moved += chunk_to_move.size
                self.device_size_by_index[max_device_index] -= chunk_to_move.size
                self.device_size_by_index[min_device_index] += chunk_to_move.size
                updated_device_by_chunk_hash[chunk_to_move.hash] = min_device_index
                # Recalculate bytes_on_max_device to pass to next iteration
                bytes_on_max_device = sum(chunk.size for chunk in chunks_on_max_device)
            # Sort devices to determine the new max and min devices after chunks were moved around
            devices_sorted = sorted(self.device_size_by_index.items(), key=lambda x: x[1])
            max_device_index, max_device_size = devices_sorted[-1]
            min_device_index, min_device_size = devices_sorted[0]
        # Use updated_device_by_chunk_hash to create new state by updating old state
        new_chunk_map = self._chunk_map.copy()
        new_device_map = self._device_map.copy()
        for chunk_hash in updated_device_by_chunk_hash:
            device_index = new_chunk_map.index(chunk_hash)
            new_device_map[device_index] = updated_device_by_chunk_hash[chunk_hash]
        self.set_state((self._device_count, new_chunk_map, new_device_map))
        return total_bytes_moved


class DeviceMapperTests(unittest.TestCase):
    """
    Methods to generate test data and test the DeviceMapper.rebalance implementation.
    """
    _seed = 0
    _device_count = 20
    _file_count = 1000000
    _device_size = int(2e12)
    _chunk_size = int(10e9)
    _max_factor_load = 0.9
    _max_mapping_expansion = 2.0
    _cache_file = 'data-{seed}.pkl'

    def test_device_mapper(self):
        """
        A basic test for the device mapper.
        :raises AssertionError: On test fail.
        """
        file_data, mapper = self._get_data(self._seed)
        old_file_data = copy.deepcopy(file_data)
        old_mapper = copy.deepcopy(mapper)

        total_device_size = self._device_count * self._device_size
        total_file_size = float(np.sum([file_size for (hash_value, file_size) in file_data]))

        print('Files: {:.2f}T'.format(float(total_file_size) / (1 << 40)))
        print('Devices: {:.2f}T'.format(total_device_size / (1 << 40)))
        print('Use: {:.3f}%'.format(100.0 * total_file_size / total_device_size))

        print('Device usage before rebalance:')
        self._print_device_usage(file_data, mapper)

        start = datetime.datetime.now()
        bytes_moved = mapper.rebalance(file_data, self._device_size, self._max_factor_load)
        end = datetime.datetime.now()
        print('Total data movement: {:.2f}G'.format(bytes_moved / float(1 << 30)))
        print('Time taken: {}'.format(end - start))

        print('Device usage after rebalance:')
        self._print_device_usage(file_data, mapper)
        self._post_rebalance_checks(old_file_data, old_mapper, file_data, mapper, bytes_moved)

    def _post_rebalance_checks(self, old_file_data, old_mapper, file_data, mapper, bytes_moved):
        """
        Check the resulting rebalance is valid.
        :param old_file_data: The old file data.
        :param old_mapper: The old device mapper.
        :param file_data: The current file data.
        :param mapper: The current device mapper.
        :param bytes_moved: The number of bytes moved.
        :raises AssertionError: On test fail.
        """
        print('Running tests...')

        # 0. Check that file_data is unchanged.
        self.assertEqual(sorted(old_file_data), sorted(file_data), 'File data should not have changed.')
        self.assertNotEqual(old_mapper.get_state(), mapper.get_state(), 'Mappings are unchanged?')

        # Compute device usage before and after rebalance.
        old_device_usages = np.zeros(self._device_count, dtype=np.int64)
        device_usages = np.zeros(self._device_count, dtype=np.int64)
        for hash_value, file_size in file_data:
            old_device_usages[old_mapper.locate_device(hash_value)] += file_size
            device_usages[mapper.locate_device(hash_value)] += file_size

        # 1. Check we haven't lost data.
        self.assertEqual(sum(old_device_usages), sum(device_usages), 'Files/chunks have been lost!')

        # 2. Check if the hash ring and chunk-to-device mapping are valid.
        self.assertEqual(mapper._chunk_map, sorted(mapper._chunk_map), 'Chunk map not valid.')
        self.assertEqual(len(mapper._chunk_map), len(mapper._device_map), 'Device map not valid.')

        # 3. Check that no new devices have been added.
        self.assertTrue(
            0 <= min(mapper._device_map) <= max(mapper._device_map) < self._device_count,
            'New devices were added.')

        # 4. Check the hash ring mapping size hasn't expanded rapidly.
        initial_chunks = self._device_count * (self._device_size // self._chunk_size)
        max_chunk_map_size = initial_chunks * self._max_mapping_expansion
        self.assertLessEqual(
            len(mapper._chunk_map), max_chunk_map_size,
            'Chunk mapping size has expanded by over {}x'.format(self._max_mapping_expansion))

        # 5. Check that the devices are balanced now.
        self.assertTrue(
            all([du <= self._device_size * self._max_factor_load for du in device_usages]),
            'Not all devices are balanced under the max factor load.')

        # 6. Check the amount of data moved.
        real_bytes_moved = sum(
            file_size for hash_value, file_size in file_data
            if old_mapper.locate_device(hash_value) != mapper.locate_device(hash_value))

        theoretical_min = self._theoretical_min_movement(old_file_data, old_mapper)
        limit = 2 * theoretical_min
        print('Data movement theoretical min: {:.2f}G, actual: {:.2f}G, max: {:.2f}G'.format(
            theoretical_min / float(1 << 30),
            real_bytes_moved / float(1 << 30),
            limit / float(1 << 30)))
        self.assertLessEqual(real_bytes_moved, limit, 'Bytes moved over theoretical limit.')
        self.assertLessEqual(real_bytes_moved, bytes_moved, 'DeviceMapper underestimating the bytes moved.')

        print('Tests passed.')


    def _theoretical_min_movement(self, file_data, mapper):
        """
        Calculate the theoretical minimum data movement.
        :param file_data: The file data.
        :param mapper: The device mapper.
        :return: The min theoretical movement between devices.
        """
        min_bytes = 0
        mapper.populate_maps(file_data)  # helper method to get chunks_by_device and device_size_by_index
        for d in mapper.chunks_by_device:
            # If the device is at capacity then for each chunk size from low to high
            # subtract the chunk from total_chunk_size until it's not, updating min_bytes with the overflow
            if (mapper.device_size_by_index[d] / self._device_size) >= self._max_factor_load:
                chunks = mapper.chunks_by_device[d]
                chunks.sort(key=lambda chunk: chunk.size)
                total_chunk_size = sum(chunk.size for chunk in chunks)
                while (total_chunk_size / self._device_size) >= self._max_factor_load:
                    chunk_size = chunks.pop(0).size
                    min_bytes += chunk_size
                    total_chunk_size -= chunk_size
        return min_bytes


    def _get_data(self, seed):
        """
        Get the dataset for a particular seed.
        :param seed: The seed the dataset is generated with.
        :return: tuple(file_data, mapper).
        """
        cache_file = self._cache_file.format(seed=seed)
        if not os.path.exists(cache_file):
            print('State file {} doesn\'t exist, creating and saving an initial data set.'.format(cache_file))
            file_data, mapper = self._create_data(seed)
            self._save_data(cache_file, file_data, mapper)
        else:
            print('Loading data set {}'.format(cache_file))
            file_data, mapper = self._load_data(cache_file)
        return file_data, mapper

    def _create_data(self, seed):
        """
        Create a representative random dataset and mapping.
        :param seed: An integer PRNG seed.
        :return: tuple(list(tuple(file_hash, file_size)), DeviceMapper)
        """
        # Repeatable
        np.random.seed(seed)

        # Create a normally distributed set of files sizes, clipped to 1B -> 100GB
        file_sizes = np.clip(np.exp(3.3 * np.random.randn(self._file_count) + 12), 1, 100 * 1 << 30).astype(np.int64)

        # Calculate file name hashes based on fictional names
        file_name_hashes = [md5_hash('file-{}'.format(file_number)) for file_number in range(self._file_count)]

        mapper = DeviceMapper()
        mapper.create(self._device_size // self._chunk_size, self._device_count)

        return list(zip(file_name_hashes, file_sizes)), mapper

    def _save_data(self, file_name, file_data, mapper):
        """
        Save the random generated dataset to disk to avoid generating it every time.
        :param file_name: The name of the data file (data.pkl).
        :param file_data: The file mappings.
        :param mapper: The mapper state to save.
        """
        with open(file_name, 'wb') as file:
            pickle.dump((file_data, mapper.get_state()), file, pickle.HIGHEST_PROTOCOL)

    def _load_data(self, file_name):
        """
        Load the dataset cache from disk.
        :param file_name: The name of the data file (data.pkl)
        :return: The file data and initial mapper state.
        """
        with open(file_name, 'rb') as file:
            file_data, mapper_state = pickle.load(file)
        mapper = DeviceMapper()
        mapper.set_state(mapper_state)
        return file_data, mapper

    def _print_device_usage(self, file_data, mapper):
        """
        Print the device usage to console.
        :param mapper: The device mapper.
        :param file_data: The file data.
        """
        used = np.zeros(self._device_count, dtype=np.int64)

        for hash_value, file_size in file_data:
            used[mapper.locate_device(hash_value)] += file_size

        for device in range(self._device_count):
            print('{:2d} {:7.2f}G : {:4.1f}%'.format(
                device,
                float(used[device]) / (1 << 30),
                100.0 * float(used[device]) / self._device_size))


def md5_hash(string):
    """
    Compute the 128-bit MD5 hash for a string.
    :param string: The string to hash.
    :return: The int value of the hash.
    """
    return int(hashlib.md5(string.encode()).hexdigest(), 16)


def main():
    """
    Run the unittests for DeviceMapper.
    """
    tests = DeviceMapperTests('test_device_mapper')
    tests.test_device_mapper()


if __name__ == '__main__':
    main()
